//
//  SettingsViewController.swift
//  Telemetry
//
//  Created by Aref Ashrafi on 12/4/18.
//  Copyright © 2018 Aref Ashrafi. All rights reserved.
//

import Foundation
import UIKit

// MARK: - SettingsView
class SettingsViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        registerSettingsBundle()
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.defaultsChanged), name: UserDefaults.didChangeNotification, object: nil)
        defaultsChanged()
    }
    // MARK:- settings bundle
    func registerSettingsBundle(){
        let appDefaults = [String:AnyObject]()
        UserDefaults.standard.register(defaults: appDefaults)
    }
    @objc func defaultsChanged(){
        if UserDefaults.standard.bool(forKey: "RedThemeKey") {
            self.view.backgroundColor = UIColor.red
            
        }
        else {
            self.view.backgroundColor = UIColor.green
        }
    }
    @IBAction func changeSettings(_ sender: Any) {
        if let url = NSURL(string: UIApplication.openSettingsURLString) as URL? {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    @IBAction func prepareforUnwind(segue: UIStoryboardSegue) {
        
    }
    
}
