//
//  ViewController.swift
//  Telemetry
//
//  Created by Martin Shamon on 11/12/18.
//  Copyright © 2018 Martin. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import CoreData
import Reachability
class WeatherViewController: UIViewController, CLLocationManagerDelegate {
    

    // Constants
    let locationManager = CLLocationManager()

    // Variables
    var weather: Weather!
    var currentLocation: CLLocation!


    @IBOutlet weak var weatherView: WeatherView!
    
    // MARK: - view for Weather
    override func viewDidLoad() {
        super.viewDidLoad()
        callDelegates()
        weather = Weather()
        setupLocation()
    }
    // MARK:- Appear
    override func viewDidAppear(_ animated: Bool) {
        locationAuthCheck()
    }
    
    // MARK:- Calling all the delegates and datasources
    func callDelegates() {
        locationManager.delegate = self
    }
    
    // MARK:- setting up some instructions for our location manager to follow.
    func setupLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()// Take Permission from the user
        locationManager.startMonitoringSignificantLocationChanges()
    }

    
    // MARK:- checking the location authentication status
    func locationAuthCheck() {
        if CLLocationManager.authorizationStatus() == .authorizedAlways {
            // Get the location from the device
            currentLocation = locationManager.location
            
            // Pass the location coord to our API

            if currentLocation == nil {
                let alert = UIAlertController(title: "Current Location", message: "Error current location null, IF DEBUGGING SET LOCATION", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }else{
                Location.sharedInstance.latitude = currentLocation.coordinate.latitude
                Location.sharedInstance.longitude = currentLocation.coordinate.longitude
                // Download the API Data
                if Connectivity.isConnectedToInternet {
                    weather.downloadCurrentWeather {
                        // Update the UI after download is completed.
                        self.updateUI()
                        self.pushNoticiation()
                    }

                    // do some tasks..
                }else {
                    self.updateUI()
                    self.pushNoticiation()
                }
            }

            

        } else {
            locationManager.requestAlwaysAuthorization() // Take Permission from the user again
            locationAuthCheck()  // Make a check
        }
    }
    // MARK: - pushNotification
    func pushNoticiation() {
        
        let content = UNMutableNotificationContent()
        content.title = getValue(value: "cityName")
        content.body = getValue(value: "currentTemp") + CELSIUS
        content.sound = UNNotificationSound.default
        
        // Time Interval 1 Hour, change here for faster Push Notification
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 120, repeats: true)
        
        let request = UNNotificationRequest(identifier: "testidentifer", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        
    }
    
    
    
    // MARK:- updates the Current weather UI in our App.
    func updateUI() {
        weatherView.cityName.text = getValue(value: "cityName")
        weatherView.currentCityTemp.text = getValue(value: "currentTemp") + CELSIUS
        weatherView.weatherType.text = getValue(value: "weatherType")
        weatherView.currentDate.text = getValue(value: "date")
        weatherView.maxTValue.text = getValue(value: "tempMax") + CELSIUS
        weatherView.minTvalue.text = getValue(value: "tempMin") + CELSIUS
        weatherView.hValue.text = getValue(value: "humidity") + PROCENT
        weatherView.pValue.text = getValue(value: "pressure") + PASCAL
        weatherView.windValue.text = getValue(value: "windSpeed") + SPEED
        weatherView.cloudValue.text = getValue(value: "clouds") + OKTA
        weatherView.weatherImage.retrieveAndCacheImage(urlString: weather.setWeatherIconURL(string: getValue(value: "weatherIcon")))
        weatherView.gifImage.image = UIImage.gif(name: getValue(value: "weatherIcon"))
        
    }
    func getValue(value: String) -> String {
        
        return UserDefaults.standard.string(forKey: value) ?? "no data"
    }
}

