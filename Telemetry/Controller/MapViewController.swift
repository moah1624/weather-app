//
//  ViewController.swift
//  location
//
//  Created by Ahmed Mohammed on 2018-11-19.
//  Copyright © 2018 Ahmed Mohammed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

// MARK: -mapControllerView
class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var modeSwitch: UISwitch!
    @IBOutlet weak var mapView: MKMapView!
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.showsUserLocation = true
        self.mapView.delegate = self
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        determineUserLocation()
    }
    // MARK: -userLocation
    func determineUserLocation()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    // MARK: - manages the location
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
    }
    // MARK: - switch mode , standard or satellite
    @IBAction func modeSwitch(_ sender: Any) {
        if mapView.mapType == MKMapType.standard{
            mapView.mapType = MKMapType.satellite
        }else{
            mapView.mapType = MKMapType.standard
        }
    }
    
}
