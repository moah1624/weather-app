//
//  WeatherView.swift
//  Telemetry
//
//  Created by Aref Ashrafi on 12/6/18.
//  Copyright © 2018 Aref Ashrafi. All rights reserved.
//

import Foundation
import UIKit
class WeatherView: UIView {
   
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var weatherType: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var currentCityTemp: UILabel!
    @IBOutlet weak var currentDate: UILabel!
    @IBOutlet weak var maxTValue: UILabel!
    @IBOutlet weak var minTvalue: UILabel!
    @IBOutlet weak var windValue: UILabel!
    @IBOutlet weak var pValue: UILabel!
    @IBOutlet weak var hValue: UILabel!
    @IBOutlet weak var cloudValue: UILabel!
    @IBOutlet weak var weatherView: UIView!
    @IBOutlet weak var gifImage: UIImageView!
    @IBOutlet weak var refreshButton: UIButton!
    
    override init(frame: CGRect){
        super.init(frame:frame)
        commonInit()
    }

    required init?(coder aCoder: NSCoder){
        super.init(coder: aCoder)
        commonInit()
    }
    func commonInit(){
        Bundle.main.loadNibNamed("WeatherView", owner: self, options: nil)
        addSubview(weatherView)
        weatherView.frame = self.bounds
        weatherView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }

}
