//
//  Location.swift
//  Telemetry
//
//  Created by Aref Ashrafi on 12/4/18.
//  Copyright © 2018 Aref Ashrafi. All rights reserved.
//

import Foundation

class Location {
    
    static var sharedInstance = Location()
    
    var longitude: Double!
    var latitude: Double!
    
}
