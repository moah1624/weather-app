//
//  Weather.swift
//  Telemetry
//
//  Created by Aref Ashrafi on 23/11/18.
//  Copyright © 2018 Aref Ashrafi. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

class Weather {
    
    private var _cityName: String!
    private var _date: String!
    private var _weatherType: String!
    private var _currentTemp: Double!
    private var _humidity: Double!
    private var _pressure: Double!
    private var _tempMin: Double!
    private var _tempMax: Double!
    private var _windSpeed: Double!
    private var _clouds: Double!
    private var _weatherIcon: String!
    private var _weatherDescription: String!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    var humidity: Double {
        if _humidity == nil {
            _humidity = 0.0
        }
        return _humidity
    }
    
    var pressure: Double {
        if _pressure == nil {
            _pressure = 0.0
        }
        return _pressure
    }
    
    var tempMin: Double {
        if _tempMin == nil {
            _tempMin = 0.0
        }
        return _tempMin
    }
    
    var tempMax: Double {
        if _tempMax == nil {
            _tempMax = 0.0
        }
        return _tempMax
    }
    
    var windSpeed: Double {
         if _windSpeed == nil {
        _windSpeed = 0.0
            
         }
        return _windSpeed
   
    }
    
    
    var currentTemp: Double {
        if _currentTemp == nil {
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    var clouds: Double {
        if _clouds == nil {
            _clouds = 0.0
        }
        return _clouds
    }
    var weatherIcon: String {
        if _weatherIcon == nil {
            _weatherIcon = ""
        }
        return _weatherIcon
    }

    /// Downloading Current Weather Data
    ///
    /// - Parameter completed: Completion Handler.
    
    func downloadCurrentWeather(completed: @escaping DownloadComplete){
        Alamofire.request(API_URL).responseJSON { (response) in
            print(response)
                let result = response.result
                let json = JSON(result.value as Any)
                self._cityName = json["name"].stringValue
                let tempDate = json["dt"].double
                let convertedDate = Date(timeIntervalSince1970: tempDate!)
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .medium
                dateFormatter.timeStyle = .none
                let currentDate = dateFormatter.string(from: convertedDate)
                self._date = "\(currentDate)"
                self._weatherType = json["weather"][0]["main"].stringValue
                let downloadedTemp = json["main"]["temp"].double
                self._currentTemp = (downloadedTemp!).rounded(toPlaces: 0)
                self._humidity = json["main"]["humidity"].double
                self._pressure = json["main"]["pressure"].double
                self._tempMin = json["main"]["temp_min"].double
                self._tempMax = json["main"]["temp_max"].double
                self._windSpeed = json["wind"]["speed"].double
                self._weatherIcon = json["weather"][0]["icon"].stringValue
                self._clouds = json["clouds"]["all"].double
                self.saveWeatherDataToUser()
                completed()
            }

        

        
    }
    func setWeatherIconURL(string: String) -> String {
        return "http://openweathermap.org/img/w/\(string).png"
    }
    func saveWeatherDataToUser(){
        UserDefaults.standard.set(cityName, forKey: "cityName")
        UserDefaults.standard.set(date, forKey: "date")
        UserDefaults.standard.set(weatherType, forKey: "weatherType")
        UserDefaults.standard.set(currentTemp, forKey: "currentTemp")
        UserDefaults.standard.set(pressure, forKey: "pressure")
        UserDefaults.standard.set(tempMax, forKey: "tempMax")
        UserDefaults.standard.set(tempMin, forKey: "tempMin")
        UserDefaults.standard.set(windSpeed, forKey: "windSpeed")
        UserDefaults.standard.set(weatherIcon, forKey: "weatherIcon")
        UserDefaults.standard.set(humidity, forKey: "humidity")
        UserDefaults.standard.set(clouds, forKey: "clouds")
    }
 
    
    
}
