//
//  Extras.swift
//  Telemetri
//
//  Created by Aref Ashrafi on 11/11/18.
//  Copyright © 2018 Aref. All rights reserved.
//

import Foundation
import Alamofire

let API_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&units=metric&appid=7c609f73c5df2dff2f32e3e3cc33cd23"

let CELSIUS = " °C"
let PROCENT = " %"
let SPEED = " m/s"
let PASCAL = " Pa"
let OKTA = " Okta"
typealias DownloadComplete = () -> ()

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
